/* PUT YOUR CODE HERE */

document.addEventListener('DOMContentLoaded', function() {

    // MODULE 1: IMAGE HOVER MODULE
    // TODO: Get add/remove functions into one toggle function and event?

    // Get element hovered element.
    const hoverArea = document.querySelector('.module-content');

    // Function to show hover effect
    function addHoverState(){
        this.classList.add('active');
    }

    // Function to hide hover effect
    function removeHoverState(){
        this.classList.remove('active');
    }

    // Listen for hover events
    hoverArea.addEventListener('mouseenter', addHoverState);
    hoverArea.addEventListener('mouseleave', removeHoverState);

    // MODULE 2: JSON MODULE
    // Create empty arrays to add the data
    const players = [];
    const stats = [];
    
    // Get the json data
    const playersData = './data/players.json';
    const statsData = './data/stats.json';
    // Location to inject data
    const jsonModuleLocation = document.querySelector('.json-module .module-content');
    // Flag to indicate arrays are empty;
    let keepCheckingData = true;

    // Fetch the json data and add to the players and stats arrays
    // TODO: Can I get both json files with one function?
    // TODO: Is there a better way to merge arrays without using underscore?

    fetch(playersData)
    .then(blob => blob.json())
    .then(data => players.push(...data.players));

    fetch(statsData)
    .then(blob => blob.json())
    .then(data => stats.push(...data.player_stats));
    

    // Function to manage data and display on the page
    function handleData(){

        // 1. Merge the two databases with underscore
        // Create new mergedArray based off id
        const mergedArray = _.map(players, function(players){
            return _.extend(players, _.omit(_.findWhere(stats, {id: players.id}), 'id'));
        });

        // 2. Filter out any players without stats
        const filteredData = mergedArray.filter(player => {
            if(player.tries || player.matches){
                return player;
            }
        });

        // 3. Display results in a table on the page

        jsonModuleLocation.innerHTML = `
            <table>
                <tr>
                    <th>Player</th>
                    <th>Matches</th>
                    <th>Tries</th>
                </tr>
                ${filteredData.map(player => 
                    `<tr>
                        <td>${player.short_name}</td>
                        <td>${player.matches}</td>
                        <td>${player.tries}</td>
                    </tr>`
                ).join('')}
            </table>
        `
    }

    // Wait until player and stats arrays are filled before running handleData function
    setInterval(() => {
        if(stats.length && players.length && keepCheckingData) { 
            handleData();
            // Flag to stop running array;
            keepCheckingData = false;
        }
    }, 50);

}, false);
